(ns malista.views-test
  (:require [clojure.test :refer :all]
            [malista.views :refer :all]
            [malista.db :as db]))

(deftest test-get-player-id-from-name

  ;; ====================
  (testing "good input"
    (let [players (list {:player_id 3, :firstname "Sven",     :surname "Birnfeld",  :sex_male true}
                        {:player_id 1, :firstname "André",    :surname "Lehmann",   :sex_male true}
                        {:player_id 2, :firstname "Anja",     :surname "Lehmann",   :sex_male false}
                        {:player_id 7, :firstname "Surender", :surname "Medipally", :sex_male true}
                        {:player_id 5, :firstname "Janine",   :surname "Nordmann",  :sex_male false}
                        {:player_id 4, :firstname "Vinatha",  :surname "Pesari",    :sex_male false}
                        {:player_id 6, :firstname "Kevin",    :surname "Werner",    :sex_male true})]
      (is (= 1 (get-player-id-from-name players "Lehmann" "André")))
      (is (= 2 (get-player-id-from-name players "Lehmann" "Anja")))
      (is (= 6 (get-player-id-from-name players "Werner" "Kevin")))
      (is (= 7 (get-player-id-from-name players "Medipally" "Surender")))
      (is (= nil (get-player-id-from-name players "Mustermann" "Max")))
      (is (= nil (get-player-id-from-name players "Max" "Mustermann")))
      (is (= nil (get-player-id-from-name players "Lehmann" "Max")))
      (is (= nil (get-player-id-from-name players "Mustermann" "Anja")))
      ))

  (testing "bad input"
    (is (= nil (get-player-id-from-name '() "Mustermann" "Max")))
    (is (= nil (get-player-id-from-name '() "" "Max")))
    (is (= nil (get-player-id-from-name '() "Mustermann" "")))
    (is (= nil (get-player-id-from-name '({:firstname "Surender", :surname "Medipally", :sex_male true}) "Medipally" "Surender")))
    
    
    ))


(deftest test-is-double-match

  ;; ====================
  (testing "double matches"
    (is (= true (is-double-match 1 2 3 4)))
    (is (= true (is-double-match 145 2342 24533 1527644)))
    )

  (testing "no double matches"
    (is (= false (is-double-match nil 2 3 4)))
    (is (= false (is-double-match 2 nil 3 4)))
    (is (= false (is-double-match 2 3 nil 4)))
    (is (= false (is-double-match 2 3 4 nil)))
    (is (= false (is-double-match 2 nil 4 nil)))
    (is (= false (is-double-match nil 4 nil 2)))
    (is (= false (is-double-match 4 nil nil 2)))
    (is (= false (is-double-match nil nil 4 2)))
    (is (= false (is-double-match 4 2 nil nil)))
    (is (= false (is-double-match 4 nil nil nil)))
    (is (= false (is-double-match nil nil nil nil)))
    )
  )

(deftest test-is-single-match

  ;; ====================
  (testing "single matches"
    (is (= true (is-single-match 1 nil 3 nil)))
    (is (= true (is-single-match 145 nil 24533 nil)))
    )

  (testing "no single matches"
    (is (= false (is-single-match nil 2 3 4)))
    (is (= false (is-single-match 2 nil 3 4)))
    (is (= false (is-single-match 2 3 nil 4)))
    (is (= false (is-single-match 2 3 4 nil)))
    (is (= false (is-single-match nil 4 nil 2)))
    (is (= false (is-single-match 4 nil nil 2)))
    (is (= false (is-single-match nil nil 4 2)))
    (is (= false (is-single-match 4 2 nil nil)))
    (is (= false (is-single-match 4 nil nil nil)))
    (is (= false (is-single-match nil nil nil nil)))
    )
  )

(defn mock-get-gender-of-player
  ""
  [player-id]
  (let [player-list {1 :male
                     2 :female
                     4 :male
                     18 :male
                     19 :male
                     23 :female
                     44 :female
                     55 :female
                     83 :male
                     90 :male
                     }]
    (val (first (filter (fn [[k v]] (= k player-id)) player-list)))
    ))

(deftest test-determine-match-type

  (binding [db/get-gender-of-player mock-get-gender-of-player]

    ;; ====================
    (testing "single matches"
      (is (= :mens-singles (determine-match-type 4 nil 18 nil)))
      (is (= :mens-singles (determine-match-type 19 nil 18 nil)))
      (is (= :womens-singles (determine-match-type 55 nil 44 nil)))
      (is (= :womens-singles (determine-match-type 23 nil 55 nil)))
      (is (= :mixed-singles (determine-match-type 55 nil 4 nil)))
      (is (= :mixed-singles (determine-match-type 18 nil 55 nil)))
      )

    
    ;; ====================
    (testing "double matches"
      (is (= :mens-doubles (determine-match-type 4 83 18 90)))
      (is (= :mens-doubles (determine-match-type 19 1 18 4)))
      (is (= :womens-doubles (determine-match-type 55 23 44 2)))
      (is (= :womens-doubles (determine-match-type 2 23 44 55)))
      (is (= :mixed-doubles (determine-match-type 55 4 44 90)))
      (is (= :mixed-doubles (determine-match-type 19 23 4 55)))
      (is (= :mixed-doubles (determine-match-type 4 55 44 90)))
      (is (= :mixed-doubles (determine-match-type 19 23 55 4)))
      )
    

    ;; ====================
    (testing "unknown matches"
      (is (= :other (determine-match-type 2 1 18 4)))
      (is (= :other (determine-match-type 1 18 2 4)))
      (is (= :other (determine-match-type 2 23 44 90)))
      (is (= :other (determine-match-type 2 90 44 23)))
      
      (is (= :other (determine-match-type 2 1 18 nil)))
      (is (= :other (determine-match-type 2 nil 18 1)))

      (is (= :other (determine-match-type 2 nil nil nil)))
      (is (= :other (determine-match-type nil 2 nil nil)))
      (is (= :other (determine-match-type nil nil 18 nil)))
      (is (= :other (determine-match-type nil nil nil 18)))

      (is (= :other (determine-match-type nil 2 18 nil)))
      (is (= :other (determine-match-type nil 2 nil 18)))
      )

    ))

(deftest test-determine-match-winner

  ;; ====================
  (testing "two set matches, player wins"
    (is (= :player (determine-match-winner {:pp1 21 :po1 15 :pp2 21 :po2 18 :pp3 nil :po3 nil})))
    (is (= :player (determine-match-winner {:pp1 22 :po1 20 :pp2 21 :po2 18 :pp3 nil :po3 nil})))
    (is (= :player (determine-match-winner {:pp1 21 :po1 15 :pp2 23 :po2 21 :pp3 nil :po3 nil})))
    (is (= :player (determine-match-winner {:pp1 21 :po1 15 :pp2 21 :po2 18})))
    )

  ;; ====================
  (testing "two set matches, opponent wins"
    (is (= :opponent (determine-match-winner {:po1 21 :pp1 15 :po2 21 :pp2 18 :pp3 nil :po3 nil})))
    (is (= :opponent (determine-match-winner {:po1 22 :pp1 20 :po2 21 :pp2 18 :pp3 nil :po3 nil})))
    (is (= :opponent (determine-match-winner {:po1 21 :pp1 15 :po2 23 :pp2 21 :pp3 nil :po3 nil})))
    (is (= :opponent (determine-match-winner {:po1 21 :pp1 15 :po2 21 :pp2 18})))
    )
  
  ;; ====================
  (testing "three set matches, player wins"
    (is (= :player (determine-match-winner {:pp1 21 :po1 15 :pp2 18 :po2 21 :pp3 21 :po3 13})))
    (is (= :player (determine-match-winner {:pp1 24 :po1 22 :pp2 18 :po2 21 :pp3 21 :po3 13})))
    (is (= :player (determine-match-winner {:pp1 21 :po1 15 :pp2 20 :po2 22 :pp3 21 :po3 13})))
    (is (= :player (determine-match-winner {:pp1 21 :po1  9 :pp2 18 :po2 21 :pp3 25 :po3 23})))
    (is (= :player (determine-match-winner {:pp1 14 :po1 21 :pp2 21 :po2 18 :pp3 21 :po3 18})))
    )
  
  ;; ====================
  (testing "three set matches, opponent wins"
    (is (= :opponent (determine-match-winner {:po1 21 :pp1 15 :po2 18 :pp2 21 :po3 21 :pp3 13})))
    (is (= :opponent (determine-match-winner {:po1 24 :pp1 22 :po2 18 :pp2 21 :po3 21 :pp3 13})))
    (is (= :opponent (determine-match-winner {:po1 21 :pp1 15 :po2 20 :pp2 22 :po3 21 :pp3 13})))
    (is (= :opponent (determine-match-winner {:po1 21 :pp1  9 :po2 18 :pp2 21 :po3 25 :pp3 23})))
    (is (= :opponent (determine-match-winner {:po1 14 :pp1 21 :po2 21 :pp2 18 :po3 21 :pp3 18})))
    )

  ;; ====================
  (testing "illegal input"
    (is (= nil (determine-match-winner {:po1 21 :pp1 15 :po2 18 :pp2 21 :po3 nil :pp3 nil})))
    (is (= nil (determine-match-winner {:po1 21 :pp1 15 :po2 nil :pp2 nil :po3 nil :pp3 nil})))
    (is (= nil (determine-match-winner {:po1 nil :pp1 nil :po2 nil :pp2 nil :po3 nil :pp3 nil})))
    (is (= nil (determine-match-winner {:po1 21 :pp1 15 :po2 18 :pp2 21 :po3 22 :pp3 22})))
    (is (= nil (determine-match-winner {:po1 21 :pp1 15 :po2 21 :pp2 21 :po3 nil :pp3 nil})))
    (is (= nil (determine-match-winner {:po1 21 :pp1 15 :po2 18 :pp2 21})))
    (is (= nil (determine-match-winner {})))
  ))
