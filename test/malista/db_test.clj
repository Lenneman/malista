

(ns malista.db-test
  (:require [clojure.test :refer :all]
            [malista.db :refer :all]
            [clojure.java.jdbc :as jdbc]
            [clojure.java.io :as io]))

(defn db-setup
  []
  (let [file-spec (str (:dbname db-spec) ".mv.db")]
    (when (.exists (io/file file-spec))
      (println "test database is existing, so deleting it ...")
      (io/delete-file file-spec)))

  (println "creating new database file and deleting the data ...")
  (setup-db)
  (jdbc/delete! db-spec :players []))

(defn db-teardown
  []
  ())

(defn db-insert-test-data-for-scores-table
  []
  (jdbc/insert-multi! db-spec :players [{:player_id 10000 :firstname "André"    :surname "Lehmann"   :sex_male true  :date_of_birth "1980-03-02" :nbv_id "04-545675"}
                                        {:player_id 10001 :firstname "Surender" :surname "Medipally" :sex_male true  :date_of_birth "1982-10-03" :nbv_id "04-355675"}
                                        {:player_id 10002 :firstname "Anja"     :surname "Lehmann"   :sex_male false :date_of_birth "1980-12-18" :nbv_id "04-545675"}
                                        {:player_id 10003 :firstname "Peter"    :surname "Krüger"    :sex_male true  :date_of_birth "1985-03-25" :nbv_id "04-348675"}
                                        {:player_id 10004 :firstname "Vinatha"  :surname "Pesari"    :sex_male false :date_of_birth "1980-05-16" :nbv_id "04-345677"}
                                        {:player_id 10005 :firstname "Janine"   :surname "Noordmann" :sex_male false :date_of_birth "1990-04-03" :nbv_id "04-845675"}
                                        {:player_id 10006 :firstname "Kerstin"  :surname "Krüger"    :sex_male false :date_of_birth "1986-11-15" :nbv_id "04-345645"}
                                        {:player_id 10007 :firstname "Robin"    :surname "Sons"      :sex_male true  :date_of_birth "1988-04-22" :nbv_id "04-335675"}])
  (jdbc/insert-multi! db-spec :results [{:points_player 18 :points_opponent 21}
                                        {:points_player 14 :points_opponent 21}
                                        {:points_player 10 :points_opponent 21}
                                        {:points_player 21 :points_opponent 15}
                                        {:points_player 21 :points_opponent 14}
                                        {:points_player 21 :points_opponent 5}
                                        {:points_player 22 :points_opponent 20}])
  )

(use-fixtures :once
  (fn [f]
    (binding [db-spec {:dbtype "h2" :dbname "./test-malista"}]
      (db-setup)
      (try
        (db-insert-test-data-for-scores-table)
        (catch Exception e (str "caught exception: " (.getMessage e))))
      (try
        (f)
        (catch Exception e (str "caught exception: " (.getMessage e))))
      (db-teardown))))


(deftest test-score-exists-in-db?
  (testing "not existing scores"
    (is (= false (score-exists-in-db? 21 13)))
    (is (= false (score-exists-in-db? 21 17)))
    (is (= false (score-exists-in-db? 21  9)))
    (is (= false (score-exists-in-db? 21  7)))
    (is (= false (score-exists-in-db? 21 19)))
    (is (= false (score-exists-in-db? 23 21)))

    (is (= false (score-exists-in-db? 13 21)))
    (is (= false (score-exists-in-db? 17 21)))
    (is (= false (score-exists-in-db?  9 21)))
    (is (= false (score-exists-in-db?  7 21)))
    (is (= false (score-exists-in-db? 19 21)))
    (is (= false (score-exists-in-db? 21 23)))

    (is (= false (score-exists-in-db? 18  5)))
    (is (= false (score-exists-in-db? 14 23)))
    (is (= false (score-exists-in-db? 10 15)))
    (is (= false (score-exists-in-db? 13 15)))
    (is (= false (score-exists-in-db? 17 14)))
    (is (= false (score-exists-in-db?  9  5)))
    (is (= false (score-exists-in-db? 13 20)))
    )

  (testing "existing scores"
    (is (= true (score-exists-in-db? 18 21)))
    (is (= true (score-exists-in-db? 14 21)))
    (is (= true (score-exists-in-db? 10 21)))
    (is (= true (score-exists-in-db? 21 15)))
    (is (= true (score-exists-in-db? 21 14)))
    (is (= true (score-exists-in-db? 21  5)))
    (is (= true (score-exists-in-db? 22 20)))
    )
  )

(deftest test-get-score-id-from-db
  (testing "not existing scores"
    (is (= nil (get-score-id-from-db 21 13)))
    (is (= nil (get-score-id-from-db 21 17)))
    (is (= nil (get-score-id-from-db 21  9)))
    (is (= nil (get-score-id-from-db 21  7)))
    (is (= nil (get-score-id-from-db 21 19)))
    (is (= nil (get-score-id-from-db 23 21)))

    (is (= nil (get-score-id-from-db 13 21)))
    (is (= nil (get-score-id-from-db 17 21)))
    (is (= nil (get-score-id-from-db  9 21)))
    (is (= nil (get-score-id-from-db  7 21)))
    (is (= nil (get-score-id-from-db 19 21)))
    (is (= nil (get-score-id-from-db 21 23)))

    (is (= nil (get-score-id-from-db 18  5)))
    (is (= nil (get-score-id-from-db 14 23)))
    (is (= nil (get-score-id-from-db 10 15)))
    (is (= nil (get-score-id-from-db 13 15)))
    (is (= nil (get-score-id-from-db 17 14)))
    (is (= nil (get-score-id-from-db  9  5)))
    (is (= nil (get-score-id-from-db 13 20)))
    )

  (testing "existing scores"
    (is (= 1 (get-score-id-from-db 18 21)))
    (is (= 2 (get-score-id-from-db 14 21)))
    (is (= 3 (get-score-id-from-db 10 21)))
    (is (= 4 (get-score-id-from-db 21 15)))
    (is (= 5 (get-score-id-from-db 21 14)))
    (is (= 6 (get-score-id-from-db 21  5)))
    (is (= 7 (get-score-id-from-db 22 20)))
    )
  )

(deftest test-add-new-score-to-db
  (testing "add when score exists"
    (is (= 1 (add-new-score-to-db 18 21)))
    (is (= 1 (add-new-score-to-db 18 21)))
    (is (= 1 (add-new-score-to-db 18 21)))
    (is (= 1 (add-new-score-to-db 18 21)))
    (is (= 4 (add-new-score-to-db 21 15)))
    (is (= 4 (add-new-score-to-db 21 15)))
    (is (= 4 (add-new-score-to-db 21 15)))
    (is (= 4 (add-new-score-to-db 21 15)))
    (is (= 7 (add-new-score-to-db 22 20)))
    (is (= 7 (add-new-score-to-db 22 20)))
    (is (= 7 (add-new-score-to-db 22 20)))
    (is (= 7 (add-new-score-to-db 22 20)))
    )

  (testing "add when score does not exist"
    (is (=  8 (add-new-score-to-db 15 21)))
    (is (=  9 (add-new-score-to-db 19 21)))
    (is (= 10 (add-new-score-to-db 6 21)))
    (is (= 11 (add-new-score-to-db 21 16)))
    (is (= 12 (add-new-score-to-db 21 18)))
    (is (= 13 (add-new-score-to-db 21 17)))
    )

  (testing "add when score exists from previous add test"
    (is (=  8 (add-new-score-to-db 15 21)))
    (is (=  9 (add-new-score-to-db 19 21)))
    (is (= 10 (add-new-score-to-db 6 21)))
    (is (= 11 (add-new-score-to-db 21 16)))
    (is (= 12 (add-new-score-to-db 21 18)))
    (is (= 13 (add-new-score-to-db 21 17)))
    )
 )

(deftest test-add-new-match-result-to-db
  (testing "add single match result"
    ;; 2 set match - player win
    (let [db-res (add-new-match-result-to-db "2018-11-16" 10000 nil 10003 nil 21 13 21 15 nil nil 2)
          query-res (jdbc/query db-spec
                                "SELECT * FROM MATCH_RESULTS WHERE MATCH_ID=1")]
      (is (= 1                                    (:match_id      (first query-res))))
      (is (= 10000                                (:player1       (first query-res))))
      (is (= nil                                  (:player2       (first query-res))))
      (is (= 10003                                (:opponent1     (first query-res))))
      (is (= nil                                  (:opponent2     (first query-res))))
      (is (= 14                                   (:result_set1   (first query-res))))   ; 21:13 is new, last entry was 21:17 with id 13
      (is (= 4                                    (:result_set2   (first query-res))))   ; 21:15 is existing with id 4
      (is (= nil                                  (:result_set3   (first query-res))))
      (is (= 2                                    (:match_type_id (first query-res))))
      (is (= (java.sql.Date/valueOf "2018-11-16") (:match_date    (first query-res))))
      )

    ;; 2 set match - opponent win
    (let [db-res (add-new-match-result-to-db "2018-01-16" 10000 nil 10005 nil 11 21 18 21 nil nil 3)
          query-res (jdbc/query db-spec
                                "SELECT * FROM MATCH_RESULTS WHERE MATCH_ID=2")]
      (is (= 2                                    (:match_id      (first query-res))))
      (is (= 10000                                (:player1       (first query-res))))
      (is (= nil                                  (:player2       (first query-res))))
      (is (= 10005                                (:opponent1     (first query-res))))
      (is (= nil                                  (:opponent2     (first query-res))))
      (is (= 15                                   (:result_set1   (first query-res))))   ; 11:21 is new, last entry was 21:13 with id 14
      (is (= 1                                    (:result_set2   (first query-res))))   ; 18:21 is existing with id 1
      (is (= nil                                  (:result_set3   (first query-res))))
      (is (= 3                                    (:match_type_id (first query-res))))
      (is (= (java.sql.Date/valueOf "2018-01-16") (:match_date    (first query-res))))
      )

    ;; 3 set match - player win
    (let [db-res (add-new-match-result-to-db "2019-01-05" 10001 nil 10005 nil 21 11 18 21 21 15 3)
          query-res (jdbc/query db-spec
                                "SELECT * FROM MATCH_RESULTS WHERE MATCH_ID=3")]
      (is (= 3                                    (:match_id      (first query-res))))
      (is (= 10001                                (:player1       (first query-res))))
      (is (= nil                                  (:player2       (first query-res))))
      (is (= 10005                                (:opponent1     (first query-res))))
      (is (= nil                                  (:opponent2     (first query-res))))
      (is (= 16                                   (:result_set1   (first query-res))))   ; 21:11 is new, last entry was 11:21 with id 15
      (is (= 1                                    (:result_set2   (first query-res))))   ; 18:21 is existing with id 1
      (is (= 4                                    (:result_set3   (first query-res))))   ; 21:15 is existing with id 4
      (is (= 3                                    (:match_type_id (first query-res))))
      (is (= (java.sql.Date/valueOf "2019-01-05") (:match_date    (first query-res))))
      )
    )

  (testing "add double match result"
    ;; 2 set match - player win
    (let [db-res (add-new-match-result-to-db "2019-11-10" 10000 10004 10003 10007 21 13 21 15 nil nil 200)
          query-res (jdbc/query db-spec
                                "SELECT * FROM MATCH_RESULTS WHERE MATCH_ID=4")]
      (is (= 4                                    (:match_id      (first query-res))))
      (is (= 10000                                (:player1       (first query-res))))
      (is (= 10004                                (:player2       (first query-res))))
      (is (= 10003                                (:opponent1     (first query-res))))
      (is (= 10007                                (:opponent2     (first query-res))))
      (is (= 14                                   (:result_set1   (first query-res))))   ; 21:13 is existing with id 14
      (is (= 4                                    (:result_set2   (first query-res))))   ; 21:15 is existing with id 4
      (is (= nil                                  (:result_set3   (first query-res))))
      (is (= 200                                  (:match_type_id (first query-res))))
      (is (= (java.sql.Date/valueOf "2019-11-10") (:match_date    (first query-res))))
      )

    ;; 3 set match - opponent win
    (let [db-res (add-new-match-result-to-db "2018-07-10" 10004 10006 10003 10007 9 21 21 15 22 24 1)
          query-res (jdbc/query db-spec
                                "SELECT * FROM MATCH_RESULTS WHERE MATCH_ID=5")]
      (is (= 5                                    (:match_id      (first query-res))))
      (is (= 10004                                (:player1       (first query-res))))
      (is (= 10006                                (:player2       (first query-res))))
      (is (= 10003                                (:opponent1     (first query-res))))
      (is (= 10007                                (:opponent2     (first query-res))))
      (is (= 17                                   (:result_set1   (first query-res))))   ; 9:21 is new, last was 21:11 with id 16
      (is (= 4                                    (:result_set2   (first query-res))))   ; 21:15 is existing with id 4
      (is (= 18                                   (:result_set3   (first query-res))))   ; 22:24 is new, last was 21:15 with 17
      (is (= 1                                    (:match_type_id (first query-res))))
      (is (= (java.sql.Date/valueOf "2018-07-10") (:match_date    (first query-res))))
      )

    )
  )

(deftest test-get-gender-of-player
  (testing "male player"
    (is (= :male (get-gender-of-player 10000)))
    (is (= :male (get-gender-of-player 10001)))
    (is (= :male (get-gender-of-player 10003)))
    (is (= :male (get-gender-of-player 10007)))
    )

  (testing "female player"
    (is (= :female (get-gender-of-player 10002)))
    (is (= :female (get-gender-of-player 10004)))
    (is (= :female (get-gender-of-player 10005)))
    (is (= :female (get-gender-of-player 10006)))
    )

  (testing "player not existing"
    (is (nil? (get-gender-of-player 9)))
    (is (nil? (get-gender-of-player 10021)))
    (is (nil? (get-gender-of-player 234)))
    )

  (testing "no player given"
    (is (nil? (get-gender-of-player nil)))
    )
  )

(deftest test-create-query-for-match-results-before-date
  (testing "normal case - 6 weeks"
    (is (= "SELECT * FROM matches WHERE match_date BETWEEN DATE '2019-02-06' AND DATE '2019-03-20' ORDER BY match_date DESC"
           (create-query-for-match-results-before-date "2019-03-20" 6)))
    (is (= "SELECT * FROM matches WHERE match_date BETWEEN DATE '2019-03-08' AND DATE '2019-04-19' ORDER BY match_date DESC"
           (create-query-for-match-results-before-date "2019-04-19" 6)))
    (is (= "SELECT * FROM matches WHERE match_date BETWEEN DATE '2018-12-04' AND DATE '2019-01-15' ORDER BY match_date DESC"
           (create-query-for-match-results-before-date "2019-01-15" 6)))
    )

  (testing "normal case - 4 weeks"
    (is (= "SELECT * FROM matches WHERE match_date BETWEEN DATE '2019-02-20' AND DATE '2019-03-20' ORDER BY match_date DESC"
           (create-query-for-match-results-before-date "2019-03-20" 4)))
    (is (= "SELECT * FROM matches WHERE match_date BETWEEN DATE '2019-03-22' AND DATE '2019-04-19' ORDER BY match_date DESC"
           (create-query-for-match-results-before-date "2019-04-19" 4)))
    (is (= "SELECT * FROM matches WHERE match_date BETWEEN DATE '2018-12-18' AND DATE '2019-01-15' ORDER BY match_date DESC"
           (create-query-for-match-results-before-date "2019-01-15" 4)))
    )

  (testing "normal case - 2 weeks"
    (is (= "SELECT * FROM matches WHERE match_date BETWEEN DATE '2019-03-06' AND DATE '2019-03-20' ORDER BY match_date DESC"
           (create-query-for-match-results-before-date "2019-03-20" 2)))
    (is (= "SELECT * FROM matches WHERE match_date BETWEEN DATE '2019-04-05' AND DATE '2019-04-19' ORDER BY match_date DESC"
           (create-query-for-match-results-before-date "2019-04-19" 2)))
    (is (= "SELECT * FROM matches WHERE match_date BETWEEN DATE '2019-01-01' AND DATE '2019-01-15' ORDER BY match_date DESC"
           (create-query-for-match-results-before-date "2019-01-15" 2)))
    )
  )
  
