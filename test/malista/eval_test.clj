(ns ^:eval-test malista.eval-test
  (:require [clojure.test :refer :all]
            [malista.eval :refer :all]))

(deftest test-det-match-winner
  (testing "player wins - 2 sets"
    (is (= :player (det-match-winner {:pp1 21 :po1 17 :pp2 21 :po2 19 :pp3 nil :po3 nil})))
    (is (= :player (det-match-winner {:pp1 21 :po1 17 :pp2 23 :po2 21 :pp3 nil :po3 nil})))
    (is (= :player (det-match-winner {:pp1 21 :po1 17 :pp2 30 :po2 29 :pp3 nil :po3 nil})))
    (is (= :player (det-match-winner {:pp1 21 :po1 19 :pp2 21 :po2 17 :pp3 nil :po3 nil})))
    (is (= :player (det-match-winner {:pp1 23 :po1 21 :pp2 21 :po2 17 :pp3 nil :po3 nil})))
    (is (= :player (det-match-winner {:pp1 30 :po1 29 :pp2 21 :po2 17 :pp3 nil :po3 nil})))
    (is (= :player (det-match-winner {:pp1 30 :po1 29 :pp2 21 :po2 17})))
    )

  (testing "player wins - 3 sets"
    (is (= :player (det-match-winner {:pp1 21 :po1 17 :pp2 18 :po2 21 :pp3 21 :po3 19})))
    (is (= :player (det-match-winner {:pp1 21 :po1 17 :pp2 18 :po2 21 :pp3 23 :po3 21})))
    (is (= :player (det-match-winner {:pp1 21 :po1 17 :pp2 18 :po2 21 :pp3 30 :po3 29})))
    )

  (testing "opponent wins - 2 sets"
    (is (= :opponent (det-match-winner {:pp1 17 :po1 21 :pp2 19 :po2 21 :pp3 nil :po3 nil})))
    (is (= :opponent (det-match-winner {:pp1 17 :po1 21 :pp2 21 :po2 23 :pp3 nil :po3 nil})))
    (is (= :opponent (det-match-winner {:pp1 17 :po1 21 :pp2 29 :po2 30 :pp3 nil :po3 nil})))
    (is (= :opponent (det-match-winner {:pp1 19 :po1 21 :pp2 17 :po2 21 :pp3 nil :po3 nil})))
    (is (= :opponent (det-match-winner {:pp1 21 :po1 23 :pp2 17 :po2 21 :pp3 nil :po3 nil})))
    (is (= :opponent (det-match-winner {:pp1 29 :po1 30 :pp2 17 :po2 21 :pp3 nil :po3 nil})))
    (is (= :opponent (det-match-winner {:pp1 17 :po1 21 :pp2 29 :po2 30})))
    )
  
  (testing "opponent wins - 3 sets"
    (is (= :opponent (det-match-winner {:pp1 21 :po1 17 :pp2 18 :po2 21 :pp3 19 :po3 21})))
    (is (= :opponent (det-match-winner {:pp1 21 :po1 17 :pp2 18 :po2 21 :pp3 21 :po3 23})))
    (is (= :opponent (det-match-winner {:pp1 21 :po1 17 :pp2 18 :po2 21 :pp3 29 :po3 30})))
    )

  (testing "invalid data"
    (is (= nil (det-match-winner {})))
    (is (= nil (det-match-winner {:pp1 21 :po1 17 :pp2 21 :po2 23 :pp3 nil :po3 nil})))
    (is (= nil (det-match-winner {:pp1 17 :po1 17 :pp2 21 :po2 23 :pp3 nil :po3 nil})))
    (is (= nil (det-match-winner {:pp1 21 :po1 17 :pp2 21 :po2 21 :pp3 nil :po3 nil})))
    (is (= nil (det-match-winner {:pp1 17 :po1 17 :pp2 21 :po2 21 :pp3 nil :po3 nil})))
    (is (= nil (det-match-winner {:pp1 21 :po1 17 :pp2 21 :po2 23 :pp3 21 :po3 21})))
    (is (= nil (det-match-winner {:pp1 21 :po1 17 :pp2 21 :po2 23 :pp3 18 :po3 18})))
    (is (= nil (det-match-winner {:pp1 21 :po1 17 :pp2 21 :po2 23 :pp3 21 :po3 nil})))
    (is (= nil (det-match-winner {:pp1 21 :po1 17 :pp2 21 :po2 23 :pp3 nil :po3 21})))
    (is (= nil (det-match-winner {:pp1 21 :po1 17 :pp3 nil :po3 nil})))
    (is (= nil (det-match-winner {:pp1 21 :po1 19 :pp2 nil :po2 nil :pp3 nil :po3 nil})))
    (is (= nil (det-match-winner {:pp1 nil :po1 nil :pp2 21 :po2 17 :pp3 nil :po3 nil})))
    (is (= nil (det-match-winner {:pp2 21 :po2 17 :pp3 nil :po3 nil})))
    )
  
  )

(deftest test-did-player-win?
  (testing "player won double"
    (is (= true (did-player-win? 710 {:player1 710 :player2 711 :opponent1 810 :opponent2 811 :pp1 21 :po1 17 :pp2 21 :po2 18 :pp3 nil :po3 nil})))
    (is (= true (did-player-win? 710 {:player1 711 :player2 710 :opponent1 810 :opponent2 811 :pp1 21 :po1 17 :pp2 21 :po2 18 :pp3 nil :po3 nil})))
    (is (= true (did-player-win? 710 {:player1 810 :player2 811 :opponent1 710 :opponent2 711 :pp1 17 :po1 21 :pp2 18 :po2 21 :pp3 nil :po3 nil})))
    (is (= true (did-player-win? 710 {:player1 810 :player2 811 :opponent1 711 :opponent2 710 :pp1 17 :po1 21 :pp2 18 :po2 21 :pp3 nil :po3 nil})))
    )
  (testing "player won single"
    (is (= true (did-player-win? 710 {:player1 710 :player2 nil :opponent1 810 :opponent2 nil :pp1 21 :po1 17 :pp2 21 :po2 18 :pp3 nil :po3 nil})))
    (is (= true (did-player-win? 710 {:player1 810 :player2 nil :opponent1 710 :opponent2 nil :pp1 17 :po1 21 :pp2 18 :po2 21 :pp3 nil :po3 nil})))
    )
  (testing "player lost double"
    (is (= false (did-player-win? 710 {:player1 710 :player2 711 :opponent1 810 :opponent2 811 :pp1 17 :po1 21 :pp2 18 :po2 21 :pp3 nil :po3 nil})))
    (is (= false (did-player-win? 710 {:player1 711 :player2 710 :opponent1 810 :opponent2 811 :pp1 17 :po1 21 :pp2 18 :po2 21 :pp3 nil :po3 nil})))
    (is (= false (did-player-win? 710 {:player1 810 :player2 811 :opponent1 710 :opponent2 711 :pp1 21 :po1 17 :pp2 21 :po2 18 :pp3 nil :po3 nil})))
    (is (= false (did-player-win? 710 {:player1 810 :player2 811 :opponent1 711 :opponent2 710 :pp1 21 :po1 17 :pp2 21 :po2 18 :pp3 nil :po3 nil})))
    )
  (testing "player lost single"
    (is (= false (did-player-win? 710 {:player1 710 :player2 nil :opponent1 810 :opponent2 nil :pp1 17 :po1 21 :pp2 18 :po2 21 :pp3 nil :po3 nil})))
    (is (= false (did-player-win? 710 {:player1 810 :player2 nil :opponent1 710 :opponent2 nil :pp1 21 :po1 17 :pp2 21 :po2 18 :pp3 nil :po3 nil})))
    )

  (testing "invalid input"
    (is (= false (did-player-win? 710 {:player1 710 :player2 nil :opponent1 810 :opponent2 nil :pp1 17 :po1 21 :pp2 21 :po2 17 :pp3 nil :po3 nil})))
    (is (= false (did-player-win? 710 {:player1 810 :player2 nil :opponent1 710 :opponent2 nil :pp1 21 :po1 17 :pp2 18 :po2 21 :pp3 13  :po3 13})))
    )
  )


(deftest test-get-matches-with-playerid
  (testing "wrong input"
    (is (= '() (get-matches-with-playerid 1 '())))
    (is (= '() (get-matches-with-playerid 1 '(1 3 5 345 2457 3684578 345))))
    (is (= '() (get-matches-with-playerid 1 '("a" "b" "c" "df"))))
    (is (= '() (get-matches-with-playerid 1 '({"a" 1, "b" 2} {"c" 5, "df" 8}))))
    (is (= '() (get-matches-with-playerid 1 '({:player1 nil, :player2 nil, :opponent1 nil, :opponent2 nil}))))
    )

  (testing "player is player1"
    (is (= '({:player1 3452, :player2 3453, :opponent1 452, :opponent2 342})
           (get-matches-with-playerid 3452 '({:player1 3452, :player2 3453, :opponent1 452, :opponent2 342}))))
    )

  (testing "player is player2"
    (is (= '({:player1 3453, :player2 3452, :opponent1 452, :opponent2 342})
           (get-matches-with-playerid 3452 '({:player1 3453, :player2 3452, :opponent1 452, :opponent2 342}))))
    )
  
  (testing "player is opponent1"
    (is (= '({:player1 452, :player2 3453, :opponent1 3452, :opponent2 342})
           (get-matches-with-playerid 3452 '({:player1 452, :player2 3453, :opponent1 3452, :opponent2 342}))))
    )
  
  (testing "player is opponent2"
    (is (= '({:player1 342, :player2 3453, :opponent1 452, :opponent2 3452})
           (get-matches-with-playerid 3452 '({:player1 342, :player2 3453, :opponent1 452, :opponent2 3452}))))
    )


  (testing "result is 0, 1, many elements from the base list"
    (def match-list '({:player1 1,  :player2 2,   :opponent1 3, :opponent2 4}
                      {:player1 5,  :player2 6,   :opponent1 7, :opponent2 8}
                      {:player1 1,  :player2 2,   :opponent1 5, :opponent2 6}
                      {:player1 1,  :player2 15,  :opponent1 5, :opponent2 6}
                      {:player1 12, :player2 nil, :opponent1 8, :opponent2 nil}
                      {:player1 1,  :player2 nil, :opponent1 2, :opponent2 nil}
                      {:player1 6,  :player2 nil, :opponent1 8, :opponent2 nil}
                      {:player1 7,  :player2 nil, :opponent1 1, :opponent2 nil}
                      ))
    (is (= '()
           (get-matches-with-playerid 18 match-list)))
    (is (= '({:player1 12, :player2 nil, :opponent1 8, :opponent2 nil})
           (get-matches-with-playerid 12 match-list)))
    (is (= '({:player1 1,  :player2 15,  :opponent1 5, :opponent2 6})
           (get-matches-with-playerid 15 match-list)))
    (is (= '({:player1 1,  :player2 2,   :opponent1 3, :opponent2 4}
             {:player1 1,  :player2 2,   :opponent1 5, :opponent2 6}
             {:player1 1,  :player2 15,  :opponent1 5, :opponent2 6}
             {:player1 1,  :player2 nil, :opponent1 2, :opponent2 nil}
             {:player1 7,  :player2 nil, :opponent1 1, :opponent2 nil})
           (get-matches-with-playerid 1 match-list)))
    (is (= '({:player1 5,  :player2 6,   :opponent1 7, :opponent2 8}
             {:player1 1,  :player2 2,   :opponent1 5, :opponent2 6}
             {:player1 1,  :player2 15,  :opponent1 5, :opponent2 6})
           (get-matches-with-playerid 5 match-list)))
    (is (= '({:player1 5,  :player2 6,   :opponent1 7, :opponent2 8}
             {:player1 7,  :player2 nil, :opponent1 1, :opponent2 nil})
           (get-matches-with-playerid 7 match-list)))
    )
  )

