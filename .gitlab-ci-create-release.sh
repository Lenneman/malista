#!/bin/bash

#echo curl \
#     --request POST \
#     --header 'Content-Type: application/json' \
#     --header 'PRIVATE-TOKEN:ZS6bm2Xbuw7arcBx-UR6' \
#     --data "{ \"name\":\"$CI_COMMIT_TAG\", \"tag_name\":\"$CI_COMMIT_TAG\", \"description\":\"$CI_COMMIT_DESCRIPTION\" }" \
#     $CI_API_V4_URL/projects/$CI_PROJECT_ID/releases


curl \
    --request POST \
    --header 'Content-Type: application/json' \
    --header 'PRIVATE-TOKEN:ZS6bm2Xbuw7arcBx-UR6' \
    --data "{ \"name\":\"$CI_COMMIT_TAG\", \"tag_name\":\"$CI_COMMIT_TAG\", \"description\":\"new release from gitlab-ci\" }" \
    $CI_API_V4_URL/projects/$CI_PROJECT_ID/releases

echo curl --request POST \
      --header "PRIVATE-TOKEN: ZS6bm2Xbuw7arcBx-UR6" \
      --data name="malista-$CI_COMMIT_TAG" \
      --data url="$CI_PROJECT_URL/-/jobs/artifacts/$CI_COMMIT_TAG/download?job=release_job" \
      $CI_API_V4_URL/projects/$CI_PROJECT_ID/releases/$CI_COMMIT_TAG/assets/links

curl --request POST \
     --header "PRIVATE-TOKEN: ZS6bm2Xbuw7arcBx-UR6" \
     --data name="malista-$CI_COMMIT_TAG" \
     --data url="$CI_PROJECT_URL/-/jobs/artifacts/$CI_COMMIT_TAG/download?job=release_job" \
     $CI_API_V4_URL/projects/$CI_PROJECT_ID/releases/$CI_COMMIT_TAG/assets/links
