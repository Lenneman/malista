# MALISTA

A web based application to enter and store match results and to get statistics about it.

MALISTA is a abbreviation for:

        MAtch LIst and STAtistics


## H2 Console

to change data in the malista database on SQL level use the H2 Console to enter and execute SQL statements on the database.
To start the H2 Console run:

   java -cp malista-<version>-standalone.jar org.h2.tools.Console -url jdbc:h2:./malista

or use the script 'start_h2_db'

## Prerequisites

You will need [Leiningen][] 2.0.0 or above installed.

[leiningen]: https://github.com/technomancy/leiningen

## Running

To start a web server for the application in productive environment, run:

   java -jar malista-<version>-standalone.jar

To start a web server for the application in development, run:

    lein ring server

## License

Copyright © 2019 André Lehmann
