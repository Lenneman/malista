@echo off


rem determine location of java
where java > java_location.txt
set /p JAVA=<java_location.txt
del java_location.txt


rem determine the latest malista jar file
dir /b malista*standalone.jar | sort /R > malista_jars.txt
set /a counter=0
for /f %%l in (malista_jars.txt) do (
    echo %%l > malista_jar.txt
    rem print out the very first line only
    if "%counter%"=="0" goto :end_reading
)
:end_reading
if exist malista_jar.txt (
    set /p MALISTA_JAR_FILE=<malista_jar.txt
) else (
    set MALISTA_JAR_FILE=""
)
del malista_jars.txt
del malista_jar.txt

if "%MALISTA_JAR_FILE%"=="" (
    echo no malista jar file could be found
    goto :end
)


rem now we are going to query the H2 db concerning the tables
set MALISTA_URL=jdbc:h2:./malista
set DB_STRUCT_OUT=malista_db_structure.txt

echo all tables > %DB_STRUCT_OUT%
echo ======================================== >> %DB_STRUCT_OUT%
%JAVA% -cp %MALISTA_JAR_FILE% org.h2.tools.Shell -url %MALISTA_URL% -sql "show tables;" >> %DB_STRUCT_OUT%

for %%t in (matches match_results match_types players results) do (
    echo. >> %DB_STRUCT_OUT%
    echo table  %%t >> %DB_STRUCT_OUT%
    echo ======================================== >> %DB_STRUCT_OUT%
    %JAVA% -cp %MALISTA_JAR_FILE% org.h2.tools.Shell -url %MALISTA_URL% -sql "show columns from %%t;" >> %DB_STRUCT_OUT%
)

:end
