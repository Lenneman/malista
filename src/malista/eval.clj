(ns malista.eval
  (:require [malista.db :as db]))


(defn det-match-winner
  "determins the match winner from the given result map. The map shall
  contain `:pp1`, `:pp2`, `:pp3`, `:po1`, `:po2`, `:po3` for the
  points of the player and opponent in first second and third set. The
  points for the third set can be omitted or can be set to `nil` if no
  third set was played.

  The function returns `:player` if the player won, `:opponent` when
  the opponent won. If the winner cannot be determined or the input is
  invalid `nil` is returned."

  [match-result]

  (try
    (if (and (nil? (:pp3 match-result))
             (nil? (:po3 match-result)))

      ;; no third set was played - check that first and second set was
      ;; won by player or opponent
      (if (and (> (:pp1 match-result) (:po1 match-result))
               (> (:pp2 match-result) (:po2 match-result)))
        :player
        ;; ok - the player didn't win, check that the opponent won two
        ;; sets
        (if (and (< (:pp1 match-result) (:po1 match-result))
                 (< (:pp2 match-result) (:po2 match-result)))
          :opponent
          ;; else - undefined match result
          nil
          ))

      ;; a third set was played, just check that - we assume that player
      ;; and opponent won one set each
      (if (> (:pp3 match-result) (:po3 match-result))
        :player
        (if (< (:pp3 match-result) (:po3 match-result))
          :opponent
          nil)))

    (catch Exception e
      nil)
    ))

(defn did-player-win?
  "Determines if a player with the given `playerid` won the match or
  not. The match result is a map containing the players (`:player1`,
  `:player2`, `:opponent1` and `:opponent1`) as well as the results of
  the sets (`:pp1`, `:pp2`, `:pp3`, `:po1`, `:po2` and `:po3`)

  returns `true` if the given palyer won the match or `false` if not"
  [playerid match-result]
  (let [winner (det-match-winner match-result)
        player-is-player? (or (= playerid (:player1 match-result))
                              (= playerid (:player2 match-result)))
        player-is-opponent? (or (= playerid (:opponent1 match-result))
                                (= playerid (:opponent2 match-result)))]
    (if (and (= winner :player) player-is-player?)
      true
      (if (and (= winner :opponent) player-is-opponent?)
        true
        false))))

(defn filter-pred-fn-for-playerid
  "returns a function that returns true if a given playerid is occuring
  in a match or false if not. This can be used as a function for
  filtering a match list by a player id"
  [playerid]
  (fn [match]
    (or (= (:player1 match) playerid)
        (= (:player2 match) playerid)
        (= (:opponent1 match) playerid)
        (= (:opponent2 match) playerid))))

(defn get-matches-with-playerid
  "takes a player id and a list of matches. Returns all matches where
  the given player id occurs as a player"
  [playerid match-list]
  (filter (filter-pred-fn-for-playerid playerid) match-list))

(defn get-count-of-won-single-matches-from-player
  ""
  [pl_id match-list]
  (let [players-matches (get-matches-with-playerid pl_id match-list)]
    (count (filter #(did-player-win? pl_id %)
                   (filter #(or (= (:match_type_id %) 1)
                                (= (:match_type_id %) 3)) players-matches)))))

(defn get-stats-of-player
  ""
  [pl-id match-list]
  (let [players-matches (get-matches-with-playerid pl-id match-list)
        players-single-matches       (filter #(or (= (:match_type_id %) db/match_type_mens_singles)
                                                  (= (:match_type_id %) db/match_type_womens_singles)
                                                  (= (:match_type_id %) db/match_type_mixed_singles)) players-matches)
        players-double-matches       (filter #(or (= (:match_type_id %) db/match_type_mens_doubles)
                                                  (= (:match_type_id %) db/match_type_womens_doubles)) players-matches)
        players-mixed-double-matches (filter #(    = (:match_type_id %) db/match_type_mixed_doubles) players-matches)
        players-won-single-matches        (filter #(did-player-win? pl-id %) players-single-matches)
        players-won-double-matches        (filter #(did-player-win? pl-id %) players-double-matches)
        players-won-mixed-double-matches  (filter #(did-player-win? pl-id %) players-mixed-double-matches)
        players-matches-cnt (count players-matches)
        players-single-matches-cnt (count players-single-matches)
        players-double-matches-cnt (count players-double-matches)
        players-mixed-double-matches-cnt (count players-mixed-double-matches)
        players-won-single-matches-cnt (count players-won-single-matches)
        players-won-double-matches-cnt (count players-won-double-matches)
        players-won-mixed-double-matches-cnt (count players-won-mixed-double-matches)
        ]
    (into {}
          {:pl-id pl-id
           :matches-cnt players-matches-cnt
           :single-matches-cnt players-single-matches-cnt
           :double-matches-cnt players-double-matches-cnt
           :mixed-double-matches-cnt players-mixed-double-matches-cnt
           :won-single-matches-cnt players-won-single-matches-cnt
           :won-double-matches-cnt players-won-double-matches-cnt
           :won-mixed-double-matches-cnt players-won-mixed-double-matches-cnt
           :won-matches (+ players-won-single-matches-cnt
                           players-won-double-matches-cnt
                           players-won-mixed-double-matches-cnt)
           :lost-single-matches-cnt (- players-single-matches-cnt players-won-single-matches-cnt)
           :lost-double-matches-cnt (- players-double-matches-cnt players-won-double-matches-cnt)
           :lost-mixed-double-matches-cnt (- players-mixed-double-matches-cnt players-won-mixed-double-matches-cnt)
           :lost-matches (- players-matches-cnt (+ players-won-single-matches-cnt
                                                   players-won-double-matches-cnt
                                                   players-won-mixed-double-matches-cnt))
           })))

(defn get-all-matches-stats-of-player
  ""
  [pl-id match-list]
  (let [players-matches         (get-matches-with-playerid pl-id match-list)
        players-won-matches     (filter #(did-player-win? pl-id %) players-matches)
        players-matches-cnt     (count players-matches)
        players-won-matches-cnt (count players-won-matches)
        ]
    (if (> players-matches-cnt 0)
      (into {}
            {:pl-id pl-id
             :matches-cnt players-matches-cnt
             :won-matches-cnt players-won-matches-cnt
             :lost-matches-cnt (- players-matches-cnt players-won-matches-cnt)
             })
      {})))

(defn get-single-stats-of-player
  ""
  [pl-id match-list]
  (let [players-matches                (get-matches-with-playerid pl-id match-list)
        players-single-matches         (filter #(or (= (:match_type_id %) db/match_type_mens_singles)
                                                    (= (:match_type_id %) db/match_type_womens_singles)
                                                    (= (:match_type_id %) db/match_type_mixed_singles)) players-matches)
        players-won-single-matches     (filter #(did-player-win? pl-id %) players-single-matches)
        players-single-matches-cnt     (count players-single-matches)
        players-won-single-matches-cnt (count players-won-single-matches)
        ]
    (if (> players-single-matches-cnt 0)
      (into {}
            {:pl-id pl-id
             :single-matches-cnt players-single-matches-cnt
             :won-single-matches-cnt players-won-single-matches-cnt
             :lost-single-matches-cnt (- players-single-matches-cnt players-won-single-matches-cnt)
             })
      {})))

