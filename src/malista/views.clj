(ns malista.views
  (:require [malista.db :as db]
            [malista.eval :as eval]
            [clojure.string :as str]
            [hiccup.page :as page]
            [ring.util.anti-forgery :as util]
            [clojure.string :as str]
            [clj-time.local]
            [clj-time.format]))

(defn gen-page-head
  [title]
  [:head
   [:title (str "Match Results Badminton: " title)]
   (page/include-css "/css/styles.css")])

(def header-links
  [:div#header-links
   "[ "
   [:a {:href "/"} "Home"]
   " | "
   [:a {:href "/add-new-player"} "Add a new Player"]
   " | "
   [:a {:href "/all-players"} "List all Players"]
   " | "
   [:a {:href "/add-new-result"} "Add a new Match Result"]
   " | "
   [:a {:href "/limited-results"} "List filtered Match Results"]
   " | "
   [:a {:href "/all-results"} "List all Match Results"]
   " | "
   [:a {:href "/stat-all-matches"} "Stat: All Matches"]
   " | "
   [:a {:href "/stat-singles"} "Stat: Singles"]
   " ]"])

(defn home-page
  []
  (page/html5
   (gen-page-head "Home")
   header-links
   [:h1 "Home"]
   [:p "Webapp to store player and match results and generate statistics about it."]))

(defn add-new-player-page
  []
  (page/html5
   (gen-page-head "Add a new Player")
   header-links
   [:h1 "Add a new Player"]
   [:form {:action "/add-new-player" :method "POST"}
    (util/anti-forgery-field) ; prevents cross-site scripting attacks
    [:p "Firstname: " [:input {:type "text" :name "fname"}]]
    [:p "Surname: "   [:input {:type "text" :name "sname"}]]
    [:p "Gender: "
     [:input {:type "radio" :name "gender" :value "male" :checked ""}] "male"
     [:input {:type "radio" :name "gender" :value "female"}] "female"]
    [:p "Date of Birth: "   [:input {:type "date" :name "dob"}]]
    [:p "NBV ID: "          [:input {:type "text" :name "nbv_id"}]]
    [:p [:input {:type "submit" :value "submit new player"}]]]))

(defn add-new-player-results-page
  [{:keys [fname sname gender dob nbv_id]}]
  (let [id (db/add-new-player-to-db fname sname gender dob nbv_id)]
    (page/html5
     (gen-page-head "Added a new Player")
     header-links
     [:h1 "Added a new Player"]
     [:p "Added [" fname ", " sname ", " gender "] (id: " id ") to the db. "])))

(defn all-players-page
  []
  (let [all-players (db/get-all-players)]
    (page/html5
     (gen-page-head "All Players in the db")
     header-links
     [:h1 "All Players"]
     [:table
      [:tr [:th "id"] [:th "surname"] [:th "firstname"] [:th "sex"] [:th "date of birth"] [:th "NBV ID"]]
      (for [player all-players]
        [:tr
         [:td (:player_id player)]
         [:td (:surname player)]
         [:td (:firstname player)]
         [:td (if (:sex_male player)
                "male"
                "female")
         [:td (:date_of_birth player)]
         [:td (:nbv_id player)]
          ]])])))

(defn get-datalist-options-with-players
  "creates a list of `option` tag of all players in the db, which ist
  then used by the `datalist` tag of html"
  []
  (let [all-players (db/get-all-players)]
    (for [player all-players]
      [:option {:value (str (:surname player) ", " (:firstname player))}])))

(def last-date-inserted
  (clj-time.format/unparse (clj-time.format/formatter "yyyy-MM-dd") (clj-time.local/local-now)))

(defn add-new-result-page
  []
    (page/html5
     (gen-page-head "Add a new Match Result")
     header-links
     [:h1 "Add a new Match Result"]
     [:form {:action "/add-new-result" :method "POST"}
      (util/anti-forgery-field) ; prevents cross-site scripting attacks
      [:datalist {:id "players"}
       (get-datalist-options-with-players)]
      [:p "Date: "      [:input {:type "date" :name "date" :value last-date-inserted}]]
      [:table
       [:tr
        [:td "Player 1:"]
        [:td [:input {:list "players" :name "player1"}]]
        [:td "Player 2:" ]
        [:td [:input {:list "players" :name "player2"}]]
        ]
       [:tr
        [:td "Opponent 1:"]
        [:td [:input {:list "players" :name "opponent1"}]]
        [:td "Opponent 2:"]
        [:td [:input {:list "players" :name "opponent2"}]]
        ]
       ]
      [:table
       [:tr
        [:td "Result Set 1: "]
        [:td [:input {:type "text" :name "rs1-p"}]]
        [:td [:input {:type "text" :name "rs1-o"}]]]
       [:tr
        [:td "Result Set 2: "]
        [:td [:input {:type "text" :name "rs2-p"}]]
        [:td [:input {:type "text" :name "rs2-o"}]]]
       [:tr
        [:td "Result Set 3: "]
        [:td [:input {:type "text" :name "rs3-p"}]]
        [:td [:input {:type "text" :name "rs3-o"}]]]]
        
      [:p [:input {:type "submit" :value "submit new result"}]]]))

(defn get-player-id-from-name
  "Determine the player id from the name.
  
  The parameters `pl-surname` and `pl-firstname` are looked up in the
  parameter `players`. The parameter `players` is a list of maps, the
  maps contain at least the keys `:surname`, `:firstname` and
  `:player_id`. The map is usually the return value of the database
  query done by `db/get-all-players`"
  [players pl-surname pl-firstname]
  (:player_id
   (first
    (filter (fn [playermap]
              (if (and (= (:surname playermap) pl-surname)
                       (= (:firstname playermap) pl-firstname))
                true
                false)) players))))

(defn is-double-match
  "Determins if a match with the given players is a double match.

  The function returns true, if all parameters are true, i.e. have a
  player id.  If one of the parameters is false or nil false is
  returned."
  [p1 p2 o1 o2]
  (if (and p1 p2 o1 o2)
    true
    false))


(defn is-single-match
  "Determins if a match with the given players is a single match.

  The function returns true, if parameters `p1` and `o1` are true,
  i.e. have a player id.  In all other cases false is returned."
  [p1 p2 o1 o2]
  (if (and p1 (nil? p2) o1 (nil? o2))
      true
      false))


(defn determine-match-type
  "Determines the match type of a given match.
  
  Dependent on the given players and opponents in parameters `p1`,
  `p2`, `o1` and `o2` and there gender the function will determine the
  match type, i.e. a single with men (`:mens-singles`) /
  women (`:womens-singles`) or both (`:mixed-singles`) or a double with
  men (`:mens-doubles`) / women (`:womens-doubles`), or a mixed
  game (`:mixed-doubles`).  If nothing of the player gender
  combinations match to a certain match type `:other` is returned.
  The parameters are the player ids from the db or `nil`
  for `p2` and `o2` if no second player played."
  [p1 p2 o1 o2]
  (if (is-single-match p1 p2 o1 o2)
    (if (and (= (db/get-gender-of-player p1) :male)
             (= (db/get-gender-of-player o1) :male))
      :mens-singles
      (if (and (= (db/get-gender-of-player p1) :female)
               (= (db/get-gender-of-player o1) :female))
        :womens-singles
        (if (or (and (= (db/get-gender-of-player p1) :male)
                     (= (db/get-gender-of-player o1) :female))
                (and (= (db/get-gender-of-player p1) :female)
                     (= (db/get-gender-of-player o1) :male)))
          :mixed-singles
          :other))
      )
    (if (is-double-match p1 p2 o1 o2)
      (if (and (= (db/get-gender-of-player p1) :male)
               (= (db/get-gender-of-player p2) :male)
               (= (db/get-gender-of-player o1) :male)
               (= (db/get-gender-of-player o2) :male))
        :mens-doubles
        (if (and (= (db/get-gender-of-player p1) :female)
                 (= (db/get-gender-of-player p2) :female)
                 (= (db/get-gender-of-player o1) :female)
                 (= (db/get-gender-of-player o2) :female))
          :womens-doubles
          (if (or (and (= (db/get-gender-of-player p1) :male)
                       (= (db/get-gender-of-player p2) :female)
                       (= (db/get-gender-of-player o1) :male)
                       (= (db/get-gender-of-player o2) :female))

                  (and (= (db/get-gender-of-player p1) :female)
                       (= (db/get-gender-of-player p2) :male)
                       (= (db/get-gender-of-player o1) :male)
                       (= (db/get-gender-of-player o2) :female))

                  (and (= (db/get-gender-of-player p1) :male)
                       (= (db/get-gender-of-player p2) :female)
                       (= (db/get-gender-of-player o1) :female)
                       (= (db/get-gender-of-player o2) :male))

                  (and (= (db/get-gender-of-player p1) :female)
                       (= (db/get-gender-of-player p2) :male)
                       (= (db/get-gender-of-player o1) :female)
                       (= (db/get-gender-of-player o2) :male)))
            :mixed-doubles
            :other)
          ))
      :other)))

(defn add-new-result-results-page
  [{:keys [date player1 player2 opponent1 opponent2 rs1-p rs1-o rs2-p rs2-o rs3-p rs3-o]}]
  
  (let [players (db/get-all-players)
        p1-surname (first (str/split player1 #", "))
        p1-fname (second (str/split player1 #", "))
        p2-surname (first (str/split player2 #", "))
        p2-fname (second (str/split player2 #", "))
        o1-surname (first (str/split opponent1 #", "))
        o1-fname (second (str/split opponent1 #", "))
        o2-surname (first (str/split opponent2 #", "))
        o2-fname (second (str/split opponent2 #", "))
        p1-id (get-player-id-from-name players p1-surname p1-fname)
        p2-id (get-player-id-from-name players p2-surname p2-fname)
        o1-id (get-player-id-from-name players o1-surname o1-fname)
        o2-id (get-player-id-from-name players o2-surname o2-fname)
        match-type (determine-match-type p1-id p2-id o1-id o2-id)
        match-type-id (db/get-match-type-id match-type)]

    (alter-var-root #'last-date-inserted (constantly date))

    (if-not (or (empty? rs3-p) (empty? rs3-o))
      (db/add-new-match-result-to-db date p1-id p2-id o1-id o2-id rs1-p rs1-o rs2-p rs2-o rs3-p rs3-o match-type-id)
      (db/add-new-match-result-to-db date p1-id p2-id o1-id o2-id rs1-p rs1-o rs2-p rs2-o nil nil match-type-id))
      
    (page/html5
     (gen-page-head "Added a new Result")
     header-links
     [:h1 "Added a new Result"]
     [:p "Added [" date ]
     [:p player1 "(" p1-id ") "]
     [:p player2 "(" p2-id ") "]
     [:p opponent1 "(" o1-id ") "]
     [:p opponent2 "(" o2-id ")"]
     [:p "results: " rs1-p ":" rs1-o ", " rs2-p ":" rs2-o ", " rs3-p ":" rs3-o]
     [:p "match-type: " match-type " ] to the db."]
     )
    ))

(defn determine-match-winner
  [mres]
  (let [;; create a vector of hash-maps with the 3 sets and the points
        ;; the player and the opponent have
        set-results (vector {:p (:pp1 mres) :o (:po1 mres)}
                            {:p (:pp2 mres) :o (:po2 mres)}
                            {:p (:pp3 mres) :o (:po3 mres)})

        ;; filter out all sets won by the player (where the point won
        ;; by player is greater than the ones from the opponent,
        ;; afterward count the filtered elements
        sets-won-by-player   (count (filter #(> (if (nil? (:p %)) 0 (:p %))
                                                (if (nil? (:o %)) 0 (:o %)))
                                                set-results))
        ;; do the same again but with sets won by the opponent
        sets-won-by-opponent (count (filter #(< (if (nil? (:p %)) 0 (:p %))
                                                (if (nil? (:o %)) 0 (:o %)))
                                                set-results))]
    (if (or (>= sets-won-by-player 2)
            (>= sets-won-by-opponent 2))
      (if (> sets-won-by-player sets-won-by-opponent)
        :player
        :opponent)
      nil)))


(defn create-table-with-match-results
  [match-results]
  [:table
   [:tr
    [:th "Date"]
    [:th.name "Player 1"]
    [:th.name "Player 2"]
    [:th.name "Opponent 1"]
    [:th.name "Opponent 2"]
    [:th "Set 1"]
    [:th "Set 2"]
    [:th "Set 3"]
    [:th "Type"]]
   (for [mres match-results]
     (let [name-player1 (if (:p1sn mres) (str (:p1sn mres) ", " (:p1fn mres)) "")
           name-player2 (if (:p2sn mres) (str (:p2sn mres) ", " (:p2fn mres)) "")
           name-opponent1 (if (:o1sn mres) (str (:o1sn mres) ", " (:o1fn mres)) "")
           name-opponent2 (if (:o2sn mres) (str (:o2sn mres) ", " (:o2fn mres)) "")]
       [:tr
        [:td (:match_date    mres)]
        (if (= (determine-match-winner mres) :player)
          (list
           [:td.winner name-player1]
           [:td.winner name-player2])
          (list
           [:td.loser  name-player1]
           [:td.loser  name-player2]))
        (if (= (determine-match-winner mres) :opponent)
          (list
           [:td.winner name-opponent1]
           [:td.winner name-opponent2])
          (list
           [:td.loser  name-opponent1]
           [:td.loser  name-opponent2]))
        [:td (str (:pp1 mres) ":" (:po1 mres))]
        [:td (str (:pp2 mres) ":" (:po2 mres))]
        [:td (str (:pp3 mres) ":" (:po3 mres))]
        [:td (:match_type mres)]
        ]))])

(defn all-results-page
  []
  (let [all-match-results (db/get-all-match-results)]
    (page/html5
     (gen-page-head "All Match Results in the db")
     header-links
     [:h1 "All Match Results"]
     (create-table-with-match-results all-match-results))))


(defn show-limited-results-page
  [{:keys [date]}]
  (let [filtered-match-results (db/get-match-results-before-date date)]
    (page/html5
     (gen-page-head "All Match Results in the db")
     header-links
     [:h1 (str "Filtered Match Results - 4 weeks before " date)]

     [:form {:action "/limited-results" :method "POST"}
      (util/anti-forgery-field) ; prevents cross-site scripting attacks
      [:p
       "Date: "
       [:input {:type "date" :name "date" :value date}]
       [:input {:type "submit" :value "update"}]]]
     (create-table-with-match-results filtered-match-results))))

(defn stat-singles2
  []
  (let [all-players (db/get-all-players)
        match-results (db/get-all-match-results-for-eval)]
    (page/html5
     (gen-page-head "Statistics: Single Matches")
     header-links
     [:h1 "Single Matches"]
     [:table
      [:tr
       [:th "id"] [:th "surname"] [:th "firstname"]
       [:th "matches"] [:th "singles"] [:th "doubles"] [:th "mixed"]
       [:th "won matches"] [:th "won singles"] [:th "won doubles"] [:th "won mixed"]
       [:th "lost matches"] [:th "lost singles"] [:th "lost doubles"] [:th "lost mixed"]
       ]
      (for [player all-players]
        (let [pl-id (:player_id player)
              player-stats (eval/get-stats-of-player pl-id match-results)]
          [:tr
           [:td pl-id]
           [:td (:surname player)]
           [:td (:firstname player)]
           [:td (:matches-cnt player-stats)]
           [:td (:single-matches-cnt player-stats)]
           [:td (:double-matches-cnt player-stats)]
           [:td (:mixed-double-matches-cnt player-stats)]
           [:td (:won-matches player-stats)]
           [:td (:won-single-matches-cnt player-stats)]
           [:td (:won-double-matches-cnt player-stats)]
           [:td (:won-mixed-double-matches-cnt player-stats)]
           [:td (:lost-matches player-stats)]
           [:td (:lost-single-matches-cnt player-stats)]
           [:td (:lost-double-matches-cnt player-stats)]
           [:td (:lost-mixed-double-matches-cnt player-stats)]
           ]))])))

(defn stat-all-matches
  []
  (let [all-players (db/get-all-players)
        match-results (db/get-all-match-results-for-eval)]
    (page/html5
     (gen-page-head "Statistics: All Matches")
     header-links
     [:h1 "All Matches"]
     [:table
      [:tr
       [:th "id"] [:th "surname"] [:th "firstname"]
       [:th "matches"]
       [:th "won matches"]
       [:th "lost matches"]
       [:th "win quote (%)"]
       ]
      (for [player all-players]
        (let [pl-id (:player_id player)
              player-stats (eval/get-all-matches-stats-of-player pl-id match-results)]
          (if (not= player-stats {})
            [:tr
             [:td pl-id]
             [:td (:surname player)]
             [:td (:firstname player)]
             [:td (:matches-cnt player-stats)]
             [:td (:won-matches-cnt player-stats)]
             [:td (:lost-matches-cnt player-stats)]
             [:td (format "%6.2f"
                          (float (* (/ (:won-matches-cnt player-stats)
                                       (:matches-cnt player-stats))
                                    100)))]
           ])))])))

(defn stat-singles
  []
  (let [all-players (db/get-all-players)
        match-results (db/get-all-match-results-for-eval)]
    (page/html5
     (gen-page-head "Statistics: Single Matches")
     header-links
     [:h1 "Single Matches"]
     [:table
      [:tr
       [:th "id"] [:th "surname"] [:th "firstname"]
       [:th "singles"]
       [:th "won singles"]
       [:th "lost singles"]
       [:th "win quote (%)"]
       ]
      (for [player all-players]
        (let [pl-id (:player_id player)
              player-stats (eval/get-single-stats-of-player pl-id match-results)]
          (if (not= player-stats {})
            [:tr
             [:td pl-id]
             [:td (:surname player)]
             [:td (:firstname player)]
             [:td (:single-matches-cnt player-stats)]
             [:td (:won-single-matches-cnt player-stats)]
             [:td (:lost-single-matches-cnt player-stats)]
             [:td (* (/ (:won-single-matches-cnt player-stats)
                        (:single-matches-cnt player-stats))
                     100)]
             ])))])))
