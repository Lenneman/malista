(ns malista.db
  (:require [clojure.java.jdbc :as jdbc]
            [clj-time.core :as time]
            [clj-time.format :as tfmt]))

(def ^:dynamic db-spec {:dbtype "h2" :dbname "./malista"})

(def db-version 5)

(def sql-stmt-create-view-str (str "CREATE VIEW matches AS "
                                   "SELECT " 
                                   "   mr.MATCH_DATE, "
                                   "   p1.firstname AS p1fn, "
                                   "   p1.surname   AS p1sn, "
                                   "   p2.firstname AS p2fn, "
                                   "   p2.surname   AS p2sn, "
                                   "   o1.firstname AS o1fn, "
                                   "   o1.surname   AS o1sn, "
                                   "   o2.firstname AS o2fn, "
                                   "   o2.surname   AS o2sn, "
                                   "   rs1.points_player AS pp1, "
                                   "   rs1.points_opponent AS po1, "
                                   "   rs2.points_player AS pp2, "
                                   "   rs2.points_opponent AS po2, "
                                   "   rs3.points_player AS pp3, "
                                   "   rs3.points_opponent AS po3, "
                                   "   mt.match_type_name AS match_type "
                                   "FROM"
                                   "   MATCH_RESULTS AS mr "
                                   "   LEFT JOIN PLAYERS AS p1  ON mr.player1  = p1.player_id "
                                   "   LEFT JOIN PLAYERS AS p2  ON mr.player2  = p2.player_id "
                                   "   LEFT JOIN PLAYERS AS o1  ON mr.opponent1 = o1.player_id "
                                   "   LEFT JOIN PLAYERS AS o2  ON mr.opponent2 = o2.player_id "
                                   "   LEFT JOIN RESULTS AS rs1 ON mr.result_set1 = rs1.result_id "
                                   "   LEFT JOIN RESULTS AS rs2 ON mr.result_set2 = rs2.result_id "
                                   "   LEFT JOIN RESULTS AS rs3 ON mr.result_set3 = rs3.result_id "
                                   "   LEFT JOIN MATCH_TYPES AS mt ON mr.match_type_id = mt.match_type_id "))

(def sql-stmt-create-matches4eval-view-str (str "CREATE VIEW matches4eval AS SELECT "
                                                " mr.MATCH_DATE, "
                                                " mr.PLAYER1, "
                                                " mr.PLAYER2, "
                                                " mr.OPPONENT1, "
                                                " mr.OPPONENT2, "
                                                " mr.MATCH_TYPE_ID, "
                                                " rs1.points_player AS pp1, "
                                                " rs1.points_opponent AS po1, "
                                                " rs2.points_player AS pp2, "
                                                " rs2.points_opponent AS po2, "
                                                " rs3.points_player AS pp3, "
                                                " rs3.points_opponent AS po3 "
                                                " FROM "
                                                " MATCH_RESULTS AS mr "
                                                " LEFT JOIN RESULTS AS rs1 ON mr.result_set1 = rs1.result_id "
                                                " LEFT JOIN RESULTS AS rs2 ON mr.result_set2 = rs2.result_id "
                                                " LEFT JOIN RESULTS AS rs3 ON mr.result_set3 = rs3.result_id "))


(defn db-update-necessary?
  ""
  []
  (let [dbversion-from-db (try
                             (first
                              (jdbc/query db-spec
                                          ["SELECT * FROM settings WHERE setting_id = ?" "1"]
                                          {:row-fn :dbversion}))
                             (catch Exception e nil))]
    (if-not (nil? dbversion-from-db)
      (println (str "db structure has version " dbversion-from-db))
      (println "db structure version couldn't be determined --> old version"))
               
    (if (= dbversion-from-db db-version) false true)))

(defn do-update-db
  "update from version 4 to version 5.

  changes:
  
  * new view 'matches4eval'"
  []

  (jdbc/execute! db-spec [sql-stmt-create-matches4eval-view-str])

  ;; last step: update dbversion in settings table
  (jdbc/update! db-spec :settings {:dbversion db-version} ["dbversion = ?" 4])
  )

(defn update-db
  ""
  []
  (if (db-update-necessary?)
    (do
      (println "updating the db structure to new version")
      (do-update-db))
    (println "db is already up to date")))


(defn setup-db
  "creates all tables and fills the table MATCH_TYPES with data
  regarding the possible match types"
  []
  (jdbc/with-db-connection [conn db-spec]
    (jdbc/db-do-commands
     conn
     [(jdbc/create-table-ddl :settings
                             [[:setting_id "identity not null primary key"]
                              [:dbversion    "bigint not null"]])
      (jdbc/create-table-ddl :players
                             [[:player_id     "identity not null primary key"]
                              [:firstname     "varchar(255)"]
                              [:surname       "varchar(255)"]
                              [:sex_male      "boolean"]
                              [:date_of_birth "date"]
                              [:nbv_id        "varchar(255)"]])

      (jdbc/create-table-ddl :results
                             [[:result_id       "identity not null primary key"]
                              [:points_player   "bigint not null"]
                              [:points_opponent "bigint not null"]])

      (jdbc/create-table-ddl :match_types
                             [[:match_type_id      "identity not null primary key"]
                              [:match_type_name    "varchar(255)"]
                              [:match_type_short   "varchar(255)"]
                              [:match_type_keyword "varchar(255)"]])

      (jdbc/create-table-ddl :match_results
                             [[:match_id      "identity not null primary key"]
                              [:match_date    "date not null"]
                              [:player1       "bigint not null"]
                              [:player2       "bigint"]
                              [:opponent1     "bigint not null"]
                              [:opponent2     "bigint"]
                              [:result_set1   "bigint not null"]
                              [:result_set2   "bigint not null"]
                              [:result_set3   "bigint"]
                              [:match_type_id "bigint"]])]))

  (jdbc/insert! db-spec :settings {:dbversion db-version})

  (jdbc/insert-multi! db-spec :match_types
                      [{:match_type_name "men's singles"
                        :match_type_short "MS"
                        :match_type_keyword "mens-singles"}
                       {:match_type_name "men's doubles"
                        :match_type_short "MD"
                        :match_type_keyword "mens-doubles"}
                       {:match_type_name "women's singles"
                        :match_type_short "WS"
                        :match_type_keyword "womens-singles"}
                       {:match_type_name "women's doubles"
                        :match_type_short "WD"
                        :match_type_keyword "womens-doubles"}
                       {:match_type_name "mixed singles"
                        :match_type_short "XS"
                        :match_type_keyword "mixed-singles"}
                       {:match_type_name "mixed doubles"
                        :match_type_short "XD"
                        :match_type_keyword "mixed-doubles"}
                       {:match_type_name "other"
                        :match_type_short "O"
                        :match_type_keyword "other"}])

  (jdbc/insert-multi! db-spec :players [{:firstname "André"    :surname "Lehmann"   :sex_male true  :date_of_birth "1980-03-02" :nbv_id "04-096359"}
                                        {:firstname "Surender" :surname "Medipally" :sex_male true  }
                                        {:firstname "Anja"     :surname "Lehmann"   :sex_male false :date_of_birth "1981-04-03" :nbv_id "04-099146"}
                                        {:firstname "Peter"    :surname "Krüger"    :sex_male true  }
                                        {:firstname "Vinatha"  :surname "Pesari"    :sex_male false }
                                        {:firstname "Janine"   :surname "Noordmann" :sex_male false }
                                        {:firstname "Kerstin"  :surname "Krüger"    :sex_male false }
                                        {:firstname "Robin"    :surname "Sons"      :sex_male true  }
                                        {:firstname "Sven"     :surname "Birnfeld"  :sex_male true                              :nbv_id "04-076028"}])

  (jdbc/execute! db-spec [sql-stmt-create-view-str]))

(def match_type_mens_singles   1)
(def match_type_mens_doubles   2)
(def match_type_womens_singles 3)
(def match_type_womens_doubles 4)
(def match_type_mixed_singles  5)
(def match_type_mixed_doubles  6)
(def match_type_other          7)

(defn add-new-player-to-db
  [fname sname sex date-of-birth nbv-id]
  (let [is-male (if (= sex "male")
                  true
                  false)
        results (jdbc/insert! db-spec :players {:firstname fname :surname sname :sex_male is-male :date_of_birth date-of-birth :nbv_id nbv-id})]
    (assert (= (count results) 1))
    (first (vals (first results)))))

(defn get-score-id-from-db
  "If a score entry is existing in the db it returns the value of
  result_id with the given values of parameters `points-pl` and
  `points-op`.  If the score is not existing it returns `nil`."
  [points-pl points-op]
  (let [qresult (jdbc/query db-spec
                            (str "SELECT RESULT_ID FROM RESULTS WHERE POINTS_PLAYER="
                                 points-pl
                                 "AND POINTS_OPPONENT="
                                 points-op))]
    (if (empty? qresult)
      nil
      (:result_id (first qresult))))) ; note: qresult is a list of a hashmap

(defn get-match-type-id
  "Determines the id of a given match type."
  [match-type-keyword]
  (let [qresult (jdbc/query db-spec
                            (str "SELECT MATCH_TYPE_ID FROM MATCH_TYPES WHERE MATCH_TYPE_KEYWORD='"
                                 (name match-type-keyword) "'"))]
    (if (empty? qresult)
      nil
      (:match_type_id (first qresult))))) ; note: qresult is a list of a hashmap

(defn score-exists-in-db?
  "Checks if a given score is already inside the db table `RESULTS`."
  [points-pl points-op]
  (not (nil? (get-score-id-from-db points-pl points-op))))

(defn add-new-score-to-db
  "If not already existing adds a new entry in the db table `RESULTS`
  with the given score for player and opponent and returns its id.  If
  the score is already existing it just returns the id of the score in
  the db."
  [points-pl points-op]
  (let [score-id (get-score-id-from-db points-pl points-op)]
    (if (nil? score-id)
      (let [iresult
            (jdbc/insert! db-spec :results {:points_player points-pl :points_opponent points-op})]
        (assert (= (count iresult) 1))
        ((keyword "scope_identity()") (first iresult))) ; note: iresult is e.g. ({:scope_identity() 28})
      score-id)))
  

(defn add-new-match-result-to-db
  "Adds a new entry for a match result in the db table MATCH_RESULTS.

  Parameters:

  `date` shall be a valid date

  `player1` and `player2` shall be the respective db-ids of the player

  `opponent1` and `opponent2` shall be the respective db-ids of the
  opponents.  If a single match was player `player2` and `opponent2`
  shall be `nil`

  `rs1-p`, `rs1-o`, `rs2-p`, `rs2-o`, `rs3-p`, `rs3-o` are the results
  of the respective sets for the player and opponent.  If no third set
  was played the parameters `rs3-p` and `rs3-o` shall be set to `nil`.

  `match-type-id` is the id of the match type from the table
  MATCH_TYPES.
  
  The function returns the id of the entry in the db."
  [date player1 player2 opponent1 opponent2 rs1-p rs1-o rs2-p rs2-o rs3-p rs3-o match-type-id]

  ;; add the result of the sets first

  (let [res-id-s1 (add-new-score-to-db rs1-p rs1-o)
        res-id-s2 (add-new-score-to-db rs2-p rs2-o)
        res-id-s3 (if-not (or (nil? rs3-p) (nil? rs3-o))
                    (add-new-score-to-db rs3-p rs3-o)
                    nil)]

    ;; add the result of the match
    (let [result
          (jdbc/insert! db-spec :match_results
                        {:match_date date
                         :player1 player1 :player2 player2
                         :opponent1 opponent1 :opponent2 opponent2
                         :result_set1 res-id-s1
                         :result_set2 res-id-s2
                         :result_set3 res-id-s3
                         :match_type_id match-type-id})]
      (assert (= (count result) 1))
      ((keyword "scope_identity()") (first result))))) ; note: result is e.g. ({:scope_identity() 28})


(defn get-all-players
  []
  (jdbc/query db-spec "SELECT * FROM PLAYERS ORDER BY SURNAME"))


(defn ^:dynamic get-gender-of-player
  "Returns the gender of a player with the id given in parameter
  `player-id`.  It returns the keywords `:male` for a male player and
  `:female` for a female player.  If the given player id is not
  included in the database then nil is returned."
  [player-id]
  (if-not (nil? player-id)
    (let [pl-sex-male? (:sex_male (first (jdbc/query 
                                          db-spec 
                                          (str "SELECT SEX_MALE FROM PLAYERS WHERE PLAYER_ID = " player-id))))]
      (if (nil? pl-sex-male?)
        nil
        (if (true? pl-sex-male?)
          :male
          :female)))
    nil))

(defn get-all-match-results
  []
  (jdbc/query db-spec (str "SELECT * FROM matches ORDER BY match_date DESC")))

(defn get-all-match-results-for-eval
  []
  (jdbc/query db-spec (str "SELECT * FROM matches4eval ORDER BY match_date DESC")))

(defn create-query-for-match-results-before-date
  [str-end-date weeks-back]
  (let [date-fmt    (tfmt/formatters :date)
        end-date    (tfmt/parse date-fmt str-end-date)
        start-date  (time/minus end-date (time/weeks weeks-back))]
    (str "SELECT * FROM matches WHERE match_date BETWEEN DATE '"
         (tfmt/unparse date-fmt start-date)
         "' AND DATE '"
         (tfmt/unparse date-fmt end-date)
         "' ORDER BY match_date DESC")))
  
(defn get-match-results-before-date
  [str-end-date]
  (jdbc/query db-spec (create-query-for-match-results-before-date str-end-date 4)))
