(ns malista.handler
  (:require [malista.views :as views]
            [malista.db :as db]
            [compojure.core :refer :all]
            [compojure.route :as route]
            [ring.adapter.jetty :as jetty]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [clojure.java.io :as io]
            [clj-time.core :as time]
            [clj-time.format :as tfmt]
            [clojure.tools.cli :refer [parse-opts]])
  (:gen-class))

(defroutes app-routes
  (GET "/"
       []
       (views/home-page))
  (GET "/add-new-player"
       []
       (views/add-new-player-page))
  (POST "/add-new-player"
        {params :params}
        (views/add-new-player-results-page params))
  (GET "/all-players"
       []
       (views/all-players-page))
  (GET "/all-results"
       []
       (views/all-results-page))
  (GET "/add-new-result"
       []
       (views/add-new-result-page))
  (POST "/add-new-result"
        {params :params}
        (views/add-new-result-results-page params))
  (GET "/limited-results"
        []
        (views/show-limited-results-page {:date (tfmt/unparse
                                                 (tfmt/formatters :date)
                                                 (time/today-at 12 00))}))
  (POST "/limited-results"
        {params :params}
        (views/show-limited-results-page params))
  (GET "/stat-all-matches"
       []
       (views/stat-all-matches))
  (GET "/stat-singles"
       []
       (views/stat-singles))
  (route/resources "/")
  (route/not-found "Not Found"))

(def app
  (wrap-defaults app-routes site-defaults))

(def cli-options
  [["-p" "--port PORT" "Port number"
    :default 5000
    :parse-fn #(Integer/parseInt %)
    :validate [#(< 0 % 0x10000) "Must be a number between 0 and 65536"]]
   ["-u" "--update" "Update DB structure"]
   ["-h" "--help"]])

(defn -main [& args]
  (let [parsed-cli-opt (parse-opts args cli-options)
        options        (:options parsed-cli-opt)
        port           (:port options)
        update?        (:update options)
        help?          (:help options)
        file-spec (str (:dbname db/db-spec) ".mv.db")]

    (when-not (.exists (io/file file-spec))
      (println "malista database file is not existing, setting up a new one ...")
      (db/setup-db))
    (if help?
      (println (:summary parsed-cli-opt))

      ;; else:
      (do
        (if update?
          (db/update-db)
          (if (db/db-update-necessary?)
            (println "WARNING: updating the db structure is necessary, consider to update using parameter '-u'")))

        (println (str "using port: " port " for web service"))
        (jetty/run-jetty #'app {:port  port
                                :join? false})))))
