#!/bin/bash

MALISTA_JAR_FILE=`ls target/malista*standalone.jar | sort | tail -1`
MALISTA_URL=jdbc:h2:./malista
DB_STRUCT_OUT=malista_db_structure.txt

echo "all tables" >> $DB_STRUCT_OUT
echo "========================================" >> $DB_STRUCT_OUT
java -cp $MALISTA_JAR_FILE org.h2.tools.Shell -url $MALISTA_URL -sql "show tables;" > $DB_STRUCT_OUT

for table in matches match_results match_types players results; do
    echo "" >> $DB_STRUCT_OUT
    echo "table " $table >> $DB_STRUCT_OUT
    echo "========================================" >> $DB_STRUCT_OUT
    java -cp $MALISTA_JAR_FILE org.h2.tools.Shell -url $MALISTA_URL -sql "show columns from $table;" >> $DB_STRUCT_OUT
done
