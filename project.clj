(defproject malista "1.0.3"
  :description "Match List and Statistics"
  :url "https://github.com/lenneman/malista"
  :min-lein-version "2.0.0"
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [compojure "1.6.1"]
                 [ring/ring-defaults "0.3.2"]
                 [ring/ring-jetty-adapter "1.7.1"]
                 [hiccup "1.0.5"]
                 [org.clojure/java.jdbc "0.7.8"]
                 [com.h2database/h2 "1.4.193"]
                 [clj-time "0.15.0"]
                 [org.clojure/tools.cli "0.4.1"]
                 ]
  :main malista.handler
  :plugins [[lein-ring "0.12.5"]]
  :ring {:handler malista.handler/app}
  :local-repo ".m2-repo"
  :test-selectors {:eval-test :eval-test}
  :profiles
  {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                        [ring/ring-mock "0.3.2"]]}})
